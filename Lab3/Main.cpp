#include <conio.h>
#include <iostream>
#include <string>
#include <fstream>
using namespace std;
void madlib(string words[10], ostream &os) {
	os << "The " << words[0] << " revolution and its consequences have been a disaster for the " << words[1] << " race. \n";
	os << "They have greatly " << words[2] << " the life expectancy of those of us who live in \"" << words[3] << "\" countries, \n";
	os << "but they have " << words[4] << " society, have made life " << words[5] << ", have subjected human beings to " << words[6] << ",\n";
	os << "have led to widespread " << words[7] << " suffering (in " << words[8] << " to physical suffering as well) \n";
	os << "and have inflicted severe damage on the " << words[9] << " world.\n";
}

int main() {
	string answers[10];
	string wordtypes[10] = { "an adjective", "an animal", "a past tense verb", "an adjective", "a past tense verb", "an adjective", "a plural noun", "an adjective",
	"a place", "a noun"};
	char yesno;

	for (int x = 0; x < 10; x++) {
		cout << "Enter " << wordtypes[x] << ". \n";
		getline(cin, answers[x]);
	}
	madlib(answers, cout);
	cout << "Would you like to save the result to a text file? Y/N: ";
	cin >> yesno;
	if (yesno == 'Y' || yesno == 'y') {
		string path = "C:\\Users\\Owner\\Documents\\results.txt";
		ofstream ofs(path);
		madlib(answers, ofs);
		ofs.close();
		cout << "Printed to C:\\Users\\Owner\\Documents\\results.txt";
	}
	_getch();
	return 0;
}